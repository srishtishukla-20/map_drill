function map(elements,callbackFb) {
    const res=[]
    for (let i=0;i<elements.length;i++){
        const currentValue=elements[i]
        const newValue=callbackFb(currentValue,i,elements);
        res.push(newValue)
    }
    return res;
}

module.exports=map;


